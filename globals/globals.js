export const LANGUAGES = {
    1:{
        acronym: "es",
        description: "Español"
    },
    2:{
        acronym: "en",
        description: "English"
    },
    3:{
        acronym: "de",
        description: "Deutsch"
    },
    4:{
        acronym: "it",
        description: "Italiano"
    },
    5:{
        acronym: "pt",
        description: "Portugës"
    },
    6:{
        acronym: "fr",
        description: "Français"
    },
    findByAcronym(acronym){
        //encuentra el acronimo y retorna el id
        for (let item in this){
            if (this[item].acronym === acronym) {
                return item
            }
        }
    },
    findById(id){
        //encuentra el id y retorna el acrónimo
        for (let item in this){
            if (Number(item) === Number(id)) {
                return this[item].acronym
            }
        }
    },
    getItems(){
        let result = {};
        for (let item in this){
            if (Number(item)>0 && Number(item)<=6) {
                result[item]=this[item]
            }
        }
        return result;
    },
};

export const USER_OCCUPATION_STATE = {
  pending: 1,
  rejected: 2,
  approved: 3,
  cancelled: 4,
  paused: 5,
  created: 6
};

export const getCookie = (cname) => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


