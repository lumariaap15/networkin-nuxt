import en from 'vee-validate/dist/locale/en';
export default {
  validation:en.messages,
  "auth": {
    "login": "Sign in",
    "recover_account":"Recover account",
    "register": "Sign up",
    "dont_have_account": "Don't you have an account yet?",
    "find_us": "Find us in",
    "register_with_google": "Sign up with Google",
    "enter_verification_code": "Please enter the verification code that was sent to your email"
  },
  "formNames":{
    "email": "Email",
    "password": "Password",
    "password_confirmation": "Password confirmation",
    "code": "Code",
    "full_name": "Full name",
    "country_of_residence": "Country of residence",
    "nick": "Nickname",
    "available": "I'm available",
    "old_password": "Current password",
    "new_password": "New password",
    "file": "Choose a file or drop it here",
    "birth": "Date of birth",
    "document": "Identification document",
    "address": "Address",
    "telephone": "Phone number",
    "politically_exposed": "Politically exposed",
    "occupation": "Occupation",
    "message_value": "Message Value",
    "call_value": "Call Value",
    "video_call_value": "Video Call Value",
    tokens:"tokens"
  },
  "buttons": {
    "submit": "Submit",
    "cancel": "Cancel",
    "submit_code": "Send verification code",
    "fill": "Refill",
    "withdraw": "Withdraw",
    "change_lang": "Switch language",
    "log_out": "Log Out",
    "update": "Update",
    "see_more": "See more",
    "post_changes": "Post changes",
    "resume": "Resume",
    "pause": "Pause"
  },
  "alerts": {
    "lang_updated": "The language was updated successfully",
    "success": "Success",
    "internal_error": "There was an error, please try again later",
    "no_permissions": "You don't have permissions",
    "update_success": "The update has been completed successfully",
    "form_errors": "There are errors on the form. Please fix them before continuing.",
    "no_records_found": "No records found matching your criteria!",
    "are_you_sure": "Are you sure?",
    "change_occupation_to_pending": "The occupation will change to status PENDING and you won't able to make any more changes",
    "delete_document": "Do you want to delete this document?",
    "logout": "Do you want to log out?",
    "delete_record": "Do you want to delete this record?"
  },
  "state": {
    "pending": "Pending",
    "rejected": "Rejected",
    "approved": "Approved",
    "created": "Created",
    "paused": "Paused"
  },
  "pages": {
    "home": {
      "label": "Home",
      "earned_tokens": "Earned Tokens",
      "all_contracts": "All Contracts",
      "average_rating": "Average Rating"
    },
    "profile": {
      "label": "Profile",
      "upload_image": "Upload image",
      "personal_info": "Personal information",
      "comments_and_ratings": "Comments and Ratings",
      "languages": "Languages in which you provide service",
      "security": "Security",
      "no_comments": "You don't have any comments yet."
    },
    "balance": {
      "label": "Balance",
      "current_balance": "Current Balance",
      "tokens_since_withdrawal": "Token transactions since last withdrawal",
      "last_withdrawal": "Last withdrawal",
      "contracts": "Contracts",
      "refills": "Refills",
      "withdrawals": "Withdrawals",
      "messages": "Messages",
      "calls": "Calls",
      "video_calls": "Video calls",
      "previous_balance": "Previous balance",
      "occupation": "Occupation",
      "date": "Date",
      "contract_type": "Contract Type",
      "spent_tokens": "Spent Tokens",
      "refill": "Refill",
      "withdrawal": "Withdrawal",
      "call": "Call",
      "video_call": "Video call",
      "type": "Type"
    },
    "occupations": {
      "label": "Occupations",
      "occupations_pending": "Pending Occupations",
      "occupations_rejected": "Rejected Occupations",
      "occupations_approved": "Approved Occupations",
      "occupations_paused": "Paused Occupations",
      "occupations_created": "Created Occupations",
      "occupations_all": "All Occupations",
      "add_occupation": "Add occupation",
      "edit_tags": "Edit Tags",
      "message_value": "Message Value",
      "call_value": "Call Value",
      "video_call_value": "Video Call Value",
      "accept_negotiation": "Accept rate negotiation",
      "save_messages_history": "Save messages history",
      "documents": "Documents",
      "document": "Document",
      "contracts_by_month": "Contracts by month",
      "documents_pending": "There are some pending documents"
    },
    "help": {
      "label": "Help"
    },
    "footer": {
      "copyright": "All rights reserved",
      "about": "We are a company dedicated to software development"
    }
  },
  "yes":"Yes",
  "no": "No",
  "required": "Required",
  "status": "Status",
  "actions": "Actions"
}
