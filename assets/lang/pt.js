import pt from 'vee-validate/dist/locale/pt_PT';
export default {
  validation:pt.messages,
  "auth": {
    "login": "Iniciar sessão",
    "recover_account":"Recuperar a conta",
    "register": "Registrar",
    "dont_have_account": "Não tens uma conta ainda?",
    "find_us": "Encontre-nos em",
    "register_with_google": "Faça login no Google",
    "enter_verification_code": "Digite o código de verificação enviado ao seu e-mail"
  },
  "formNames":{
    "email": "Correio eletrônico",
    "password": "Senha",
    "password_confirmation": "ConfirmaÇão Da Senha",
    "code": "Código",
    "full_name": "Nome completo",
    "country_of_residence": "País de residência",
    "nick": "Apelido",
    "available": "Eu estou disponível",
    "old_password": "Senha atual",
    "new_password": "Nova senha",
    "file": "Escolha um arquivo ou solte-o aqui",
    "birth": "Data de nascimento",
    "document": "Documento de identificação",
    "address": "Endereço",
    "telephone": "Celular",
    "politically_exposed": "Politicamente exposto",
    "occupation": "Ocupação",
    "message_value": "Valor da mensagem",
    "call_value": "Valor da chamada",
    "video_call_value": "Valor da videochamada",
    tokens:"tokens"
  },
  "buttons": {
    "submit": "Enviar",
    "cancel": "Cancelar",
    "submit_code": "Eviar código de verificação",
    "fill": "Reabastecer",
    "withdraw": "Retirar",
    "change_lang": "Mudar de idioma",
    "log_out": "Sair",
    "update": "Atualizar",
    "see_more": "Ver mais",
    "post_changes": "Publicar alterações",
    "resume": "Prosseguir",
    "pause": "Pausar"
  },
  "alerts": {
    "lang_updated": "O idioma foi atualizado corretamente",
    "success": "Sucesso",
    "internal_error": "Houve um erro, por favor tente novamente mais tarde",
    "no_permissions": "Você não tem permissões",
    "update_success": "A atualização foi concluída com sucesso",
    "form_errors": "Existem erros no formulário. Corrija-os antes de continuar.",
    "no_records_found": "Não foram encontrados registros correspondentes aos seus critérios!",
    "are_you_sure": "Você tem certeza?",
    "change_occupation_to_pending": "A ocupação mudará para o status PENDENTE e você não poderá fazer mais alterações",
    "delete_document": "Você quer deletar este documento?",
    "logout": "Você quer sair?",
    "delete_record": "Você quer deletar este registro?"
  },
  "state": {
    "pending": "Pendente",
    "rejected": "Rejeitado",
    "approved": "Aprovado",
    "created": "Criado",
    "paused": "Pausado"
  },
  "pages": {
    "home": {
      "label": "Home",
      "earned_tokens": "Tokens ganhos",
      "all_contracts": "Todos os contratos",
      "average_rating": "Classificação média"
    },
    "profile": {
      "label": "Perfil",
      "upload_image": "Enviar Imagem",
      "personal_info": "Informação pessoal",
      "comments_and_ratings": "Comentários e Avaliações",
      "languages": "Idiomas em que você presta serviços",
      "security": "Segurança",
      "no_comments": "Você ainda não tem nenhum comentário."
    },
    "balance": {
      "label": "Saldo",
      "current_balance": "Saldo atual",
      "tokens_since_withdrawal": "Transações de token desde a última retirada",
      "last_withdrawal": "Última retirada",
      "contracts": "Contratos",
      "refills": "Recargas",
      "withdrawals": "Retirada",
      "messages": "Mensagens",
      "calls": "Chamadas",
      "video_calls": "Chamada de Vídeo",
      "previous_balance": "Saldo prévio",
      "occupation": "Ocupação",
      "date": "Encontro",
      "contract_type": "Tipo de contrato",
      "spent_tokens": "Tokens gastos",
      "refill": "Recarga",
      "withdrawal": "Retirada",
      "call": "Chamada",
      "video_call": "Videochamada",
      "type": "Tipo"
    },
    "occupations": {
      "label": "Ocupação",
      "occupations_pending": "Ocupações pendentes",
      "occupations_rejected": "Ocupações Rejeitadas",
      "occupations_approved": "Ocupações Aprovadas",
      "occupations_paused": "Ocupações em pausa",
      "occupations_created": "Profissões Criadas",
      "occupations_all": "Todas as ocupações",
      "add_occupation": "Adicionar ocupação",
      "edit_tags": "Editar Tags",
      "message_value": "Valor da mensagem",
      "call_value": "Valor da chamada",
      "video_call_value": "Valor da videochamada",
      "accept_negotiation": "Aceitar negociação de taxa",
      "save_messages_history": "Salvar histórico de mensagens",
      "documents": "Documents",
      "document": "Documento",
      "contracts_by_month": "Contratos por mês",
      "documents_pending": "Existem alguns documentos pendentes"
    },
    "help": {
      "label": "Ajuda"
    },
    "footer": {
      "copyright": "Todos os direitos reservados",
      "about": "Somos uma empresa dedicada ao desenvolvimento de software"
    }
  },
  "yes":"Sim",
  "no": "Não",
  "required": "Requeridos",
  "status": "Status",
  "actions": "Ações"
}
