import de from 'vee-validate/dist/locale/de';
export default {
  validation:de.messages,
  auth: {
    login: "Anmeldung",
    recover_account:"Account wiederherstellen",
    register: "Registrieren",
    dont_have_account: "Hast du noch keinen Account?",
    find_us: "Finden Sie uns in",
    register_with_google: "Anmeldung mit Google",
    enter_verification_code: "Bitte geben Sie den Bestätigungscode ein, der an Ihre E-Mail gesendet wurde"
  },
  formNames:{
    email: "Email",
    password: "Passwort",
    password_confirmation: "Passwort Bestätigung",
    code: "Code",
    full_name: "Vollständiger Name",
    country_of_residence: "Land des Wohnsitzes",
    nick: "Spitzname",
    available: "Ich bin verfügbar",
    old_password: "Derzeitiges Passwort",
    new_password: "Neues Kennwort",
    file: "Wählen Sie eine Datei aus oder legen Sie sie hier ab",
    birth: "Geburtsdatum",
    document: "Ausweis",
    address: "Adresse",
    telephone: "Handynummer",
    politically_exposed: "Politisch ausgesetzt",
    occupation: "Besetzung",
    message_value: "Nachrichtenwert",
    call_value: "Wert aufrufen",
    video_call_value: "Videoanrufwert",
    tokens:"tokens"
  },
  buttons: {
    submit: "Senden",
    cancel: "Stornieren",
    submit_code: "Bestätigungscode senden",
    fill: "nachfüllen",
    withdraw: "zurückziehen",
    change_lang: "Sprache wechseln",
    log_out: "Ausloggen",
    update: "Aktualisieren",
    see_more: "Mehr sehen",
    post_changes: "Änderungen posten",
    resume: "Fortsetzen",
    pause: "Pause"
  },
  alerts: {
    lang_updated: "Die Sprache wurde korrekt aktualisiert",
    success: "Gelingen",
    internal_error: "Es ist ein Fehler aufgetreten. Bitte versuchen Sie es später erneut",
    no_permissions: "Sie haben keine Berechtigungen",
    update_success: "Das Update wurde erfolgreich abgeschlossen",
    form_errors: "Das Formular enthält Fehler. Bitte beheben Sie sie, bevor Sie fortfahren.",
    no_records_found: "Es wurden keine Datensätze gefunden, die Ihren Kriterien entsprechen!",
    are_you_sure: "Bist du sicher?",
    change_occupation_to_pending: "Der Beruf ändert sich in den Status PENDING und Sie können keine weiteren Änderungen vornehmen",
    delete_document: "Möchten Sie dieses Dokument löschen?",
    logout: "Möchten Sie sich abmelden?",
    delete_record: "Möchten Sie diesen Datensatz löschen?"
  },
  state: {
    pending: "Anstehend",
    rejected: "Abgelehnt",
    approved: "Zugelassener",
    created: "Erstellt",
    paused: "Angehalten"
  },
  pages: {
    home: {
      label: "Instrumententafel",
      earned_tokens: "Verdiente Tokens",
      all_contracts: "Alle Verträge",
      average_rating: "Durchschnittliche Bewertung"
    },
    profile: {
      label: "Profil",
      upload_image: "Bild hochladen",
      personal_info: "Persönliche Informationen",
      comments_and_ratings: "Kommentare und Bewertungen",
      languages: "Sprachen, in denen Sie Dienstleistungen erbringen",
      security: "Sicherheit",
      no_comments: "Sie haben noch keine Kommentare."
    },
    balance: {
      label: "Saldo",
      current_balance: "Aktueller Saldo",
      tokens_since_withdrawal: "Token Transaktionen seit der letzten Auszahlung",
      last_withdrawal: "Letzte Auszahlung",
      contracts: "Verträge",
      refills: "Nachfüllungen",
      withdrawals: "Abhebungen",
      messages: "Textnachrichten",
      calls: "Sprachanrufe",
      video_calls: "Videoanrufe",
      previous_balance: "Vorherige Saldo",
      occupation: "Besetzung",
      date: "Datum",
      contract_type: "Vertragstyp",
      spent_tokens: "Ausgegebene Token",
      refill: "Nachfüllung",
      withdrawal: "Abhebung",
      call: "Anruf",
      video_call: "Videoanruf",
      type: "Art"
    },
    occupations: {
      label: "Berufe",
      occupations_pending: "Anstehende Berufe",
      occupations_rejected: "Abgelehnte Berufe",
      occupations_approved: "Zugelassene Berufe",
      occupations_paused: "Angehaltene Berufe",
      occupations_created: "Erstellte Berufe",
      occupations_all: "Alle Berufe",
      add_occupation: "Beruf hinzufügen",
      edit_tags: "Tags bearbeiten",
      message_value: "Nachrichtenwert",
      call_value: "Wert aufrufen",
      video_call_value: "Videoanrufwert",
      accept_negotiation: "Tarifverhandlung akzeptieren",
      save_messages_history: "Nachrichtenverlauf speichern",
      documents: "Unterlagen",
      document: "Dokument",
      contracts_by_month: "Verträge nach Monat",
      documents_pending: "Es sind einige Dokumente ausstehend"
    },
    help: {
      label: "Hilfe"
    },
    footer: {
      copyright: "Alle Rechte vorbehalten",
      about: "Wir sind ein Unternehmen, das sich der Softwareentwicklung widmet"
    }
  },
  yes:"Ja",
  no: "Nein",
  required: "Erforderlich",
  status: "Status",
  actions: "Aktionen"
}
