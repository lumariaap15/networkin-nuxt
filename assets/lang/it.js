import it from 'vee-validate/dist/locale/it';
export default {
  validation:it.messages,
  "auth": {
  "login": "Accesso",
    "recover_account":"Recuperare conto",
    "register": "Registrare",
    "dont_have_account": "Non hai ancora un account?",
    "find_us": "Trovaci in",
    "register_with_google": "Accedi con google",
    "enter_verification_code": "Inserisci il codice di verifica che è stato inviato alla tua email"
},
  "formNames":{
  "email": "E-mail",
    "password": "Parola d'ordine",
    "password_confirmation": "Conferma password",
    "code": "Codice",
    "full_name": "Nome e cognome",
    "country_of_residence": "Paese di residenza",
    "nick": "Soprannome",
    "available": "Sono disponibile",
    "old_password": "Password attuale",
    "new_password": "Nuova password",
    "file": "Scegli un file o rilascialo qui",
    "birth": "Data di nascita",
    "document": "Documento di identificazione",
    "address": "Indirizzo",
    "telephone": "Cellulare",
    "politically_exposed": "Politicamente esposto",
    "occupation": "Occupazione",
    "message_value": "Valore del messaggio",
    "call_value": "Valore di chiamata",
    "video_call_value": "Valore della videochiamata",
    tokens:"tokens"
},
  "buttons": {
  "submit": "Spedire",
    "cancel": "Cancellare",
    "submit_code": "Invia il codice di verifica",
    "fill": "Ricaricare",
    "withdraw": "Ritirare",
    "change_lang": "Cambia lingua",
    "log_out": "Disconnettersi",
    "update": "Aggiornare",
    "see_more": "Vedi altro",
    "post_changes": "Pubblica modifiche",
    "resume": "Riprendere",
    "pause": "Fermarsi"
},
  "alerts": {
  "lang_updated": "La lingua è stata aggiornata correttamente",
    "success": "Successo",
    "internal_error": "Si è verificato un errore, riprova più tardi",
    "no_permissions": "Non hai i permessi",
    "update_success": "L'aggiornamento è stato completato con successo",
    "form_errors": "Ci sono errori nel modulo. Correggili prima di continuare.",
    "no_records_found": "Nessun record trovato corrispondente ai tuoi criteri!",
    "are_you_sure": "Sei sicuro?",
    "change_occupation_to_pending": "L'occupazione passerà allo stato IN SOSPESO e non potrai più apportare modifiche",
    "delete_document": "Vuoi eliminare questo documento?",
    "logout": "Vuoi disconnetterti?",
    "delete_record": "Vuoi eliminare questo record?"
},
  "state": {
  "pending": "In sospeso",
    "rejected": "Respinto",
    "approved": "Approvato",
    "created": "Creato",
    "paused": "In pausa"
},
  "pages": {
  "home": {
    "label": "Home",
      "earned_tokens": "Token guadagnati",
      "all_contracts": "Tutti i contratti",
      "average_rating": "Voto medio"
  },
  "profile": {
    "label": "Profilo",
      "upload_image": "Carica immagine",
      "personal_info": "Informazione personale",
      "comments_and_ratings": "Commenti e voti",
      "languages": "Lingue in cui fornisci il servizio",
      "security": "Sicurezza",
      "no_comments": "Non hai ancora commenti."
  },
  "balance": {
    "label": "Saldo",
      "current_balance": "Saldo corrente",
      "tokens_since_withdrawal": "Transazioni Token dall'ultimo ritiro",
      "last_withdrawal": "Ultimo ritiro",
      "contracts": "Contratti",
      "refills": "Ricariche",
      "withdrawals": "Ritiri",
      "messages": "Messaggi",
      "calls": "Chiamate",
      "video_calls": "Videochiamate",
      "previous_balance": "Saldo precedente",
      "occupation": "Occupazione",
      "date": "Data",
      "contract_type": "Tipo di contratto",
      "spent_tokens": "Token spesi",
      "refill": "Ricarica",
      "withdrawal": "Ritiro",
      "call": "Chiamata",
      "video_call": "Videochiamata",
      "type": "Tipo"
  },
  "occupations": {
    "label": "Occupazioni",
      "occupations_pending": "Occupazioni in sospeso",
      "occupations_rejected": "Professioni respinte",
      "occupations_approved": "Professioni approvate",
      "occupations_paused": "Occupazioni in pausa",
      "occupations_created": "Occupazioni create",
      "occupations_all": "Tutte le professioni",
      "add_occupation": "Aggiungi occupazione",
      "edit_tags": "Modificare i tag",
      "message_value": "Valore del messaggio",
      "call_value": "Valore di chiamata",
      "video_call_value": "Valore della videochiamata",
      "accept_negotiation": "Accetta la negoziazione della tariffa",
      "save_messages_history": "Salva la cronologia dei messaggi",
      "documents": "Documenti",
      "document": "Documento",
      "contracts_by_month": "Contratti mensili",
      "documents_pending": "Ci sono alcuni documenti in sospeso"
  },
  "help": {
    "label": "Aiuto"
  },
  "footer": {
    "copyright": "Tutti i diritti riservati",
      "about": "Siamo una società dedicata allo sviluppo del software"
  }
},
  "yes":"Sì",
  "no": "No",
  "required": "Necessario",
  "status": "Stato",
  "actions": "Azioni"
}
