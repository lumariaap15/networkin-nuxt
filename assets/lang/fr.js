import fr from 'vee-validate/dist/locale/fr';
export default {
  validation:fr.messages,
  "auth": {
    "login": "Commencer la session",
    "recover_account":"Récupérer compte",
    "register": "Enregister",
    "dont_have_account": "Vous n'avez pas encore de compte?",
    "find_us": "Retrouvez-nous dans",
    "register_with_google": "Se connecter avec Google",
    "enter_verification_code": "Veuillez saisir le code de vérification qui a été envoyé à votre adresse e-mail"
  },
  "formNames":{
    "email": "Courrier électronique",
    "password": "Mot de passe",
    "password_confirmation": "Confirmation mot de passe",
    "code": "Code",
    "full_name": "Nom complet",
    "country_of_residence": "Pays de résidence",
    "nick": "Surnom",
    "available": "Je suis disponible",
    "old_password": "Mot de passe actuel",
    "new_password": "Nouveau mot de passe",
    "file": "Choisissez un fichier ou déposez-le ici",
    "birth": "Date de naissance",
    "document": "Document d'identification",
    "address": "Adresse",
    "telephone": "Téléphone",
    "politically_exposed": "Politiquement exposé",
    "occupation": "Profession",
    "message_value": "Valeur du message",
    "call_value": "Valeur d'appel",
    "video_call_value": "Valeur de l'appel vidéo",
    tokens:"tokens"
  },
  "buttons": {
    "submit": "Envoyer",
    "cancel": "Annuler",
    "submit_code": "Envoyer le code de vérification",
    "fill": "Recharger",
    "withdraw": "Retirer",
    "change_lang": "Changer de langue",
    "log_out": "Se déconnecter",
    "update": "Mettre à jour",
    "see_more": "En savoir plus",
    "post_changes": "Publier les modifications",
    "resume": "Recommencer",
    "pause": "Pauser"
  },
  "alerts": {
    "lang_updated": "La langue a été mise à jour correctement",
    "success": "Succès",
    "internal_error": "Il y a eu une erreur, veuillez réessayer plus tard",
    "no_permissions": "Tu n'as pas d'autorisations",
    "update_success": "La mise à jour a été effectuée avec succès",
    "form_errors": "Il y a des erreurs sur le formulaire. Veuillez les corriger avant de continuer.",
    "no_records_found": "Aucun enregistrement trouvé correspondant à vos critères!",
    "are_you_sure": "Êtes-vous sûr?",
    "change_occupation_to_pending": "L'occupation passera au statut EN ATTENTE et vous ne pourrez plus apporter de modifications",
    "delete_document": "Voulez-vous supprimer ce document?",
    "logout": "Voulez-vous vous déconnecter?",
    "delete_record": "Voulez-vous supprimer cet enregistrement?"
  },
  "state": {
    "pending": "En attente",
    "rejected": "Rejeté",
    "approved": "Approuvé",
    "created": "Créée",
    "paused": "Interrompu"
  },
  "pages": {
    "home": {
      "label": "Accueil",
      "earned_tokens": "Gagné Tokens",
      "all_contracts": "Tous les contrats",
      "average_rating": "Note moyenne"
    },
    "profile": {
      "label": "Profil",
      "upload_image": "Télécharger l'image",
      "personal_info": "Renseignements personnels",
      "comments_and_ratings": "Commentaires et évaluations",
      "languages": "Langues dans lesquelles vous fournissez le service",
      "security": "Sécurité",
      "no_comments": "Vous n'avez pas encore de commentaire."
    },
    "balance": {
      "label": "Solde",
      "current_balance": "Solde actuel",
      "tokens_since_withdrawal": "Transactions de Tokens depuis le dernier retrait",
      "last_withdrawal": "Dernier retrait",
      "contracts": "Contrats",
      "refills": "Recharges",
      "withdrawals": "Retraits",
      "messages": "Messages",
      "calls": "Appels",
      "video_calls": "Appels vidéo",
      "previous_balance": "Solde précédent",
      "occupation": "Profession",
      "date": "Date",
      "contract_type": "Type de contrat",
      "spent_tokens": "Tokens dépensés",
      "refill": "Recharge",
      "withdrawal": "Retrait",
      "call": "Appel",
      "video_call": "Appel vidéo",
      "type": "Type"
    },
    "occupations": {
      "label": "Professions",
      "occupations_pending": "Professions en attente",
      "occupations_rejected": "Professions rejetées",
      "occupations_approved": "Professions approuvées",
      "occupations_paused": "Professions interrompues",
      "occupations_created": "Professions créées",
      "occupations_all": "Toutes les professions",
      "add_occupation": "Ajouter une profession",
      "edit_tags": "Étiquettes d'édition",
      "message_value": "Valeur du message",
      "call_value": "Valeur d'appel",
      "video_call_value": "Valeur de l'appel vidéo",
      "accept_negotiation": "Accepter la négociation des tarifs",
      "save_messages_history": "Enregistrer l'historique des messages",
      "documents": "Des documents",
      "document": "Document",
      "contracts_by_month": "Contrats par mois",
      "documents_pending": "Il y a des documents en attente"
    },
    "help": {
      "label": "Aidez-moi"
    },
    "footer": {
      "copyright": "Tous les droits sont réservés",
      "about": "Nous sommes une entreprise dédiée au développement de logiciels"
    }
  },
  "yes":"Oui",
  "no": "Non",
  "required": "Obligatoire",
  "status": "Statut",
  "actions": "Actions"
}
