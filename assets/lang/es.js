import es from 'vee-validate/dist/locale/es';
export default {
  validation:es.messages,
  "auth": {
    "login": "Iniciar Sesión",
    "recover_account":"Recuperar cuenta",
    "register": "Registrarse",
    "dont_have_account": "¿Aún no tienes una cuenta?",
    "find_us": "Encuéntranos en",
    "register_with_google": "Continuar con Google",
    "enter_verification_code": "Por favor ingresa el código de verificación que se envió a su correo electrónico"
  },
  "formNames":{
    "email": "Correo electrónico",
    "password": "Contraseña",
    "password_confirmation": "Confirmar contraseña",
    "code": "Código",
    "full_name": "Nombre completo",
    "country_of_residence": "País de residencia",
    "nick": "Apodo",
    "available": "Estoy disponible",
    "old_password": "Contraseña actual",
    "new_password": "Contraseña nueva",
    "file": "Selecciona una imagen o arrástrala aquí",
    "birth": "Fecha de nacimiento",
    "document": "Documento de identidad",
    "address": "Dirección",
    "telephone": "Celular",
    "politically_exposed": "Políticamente expuesto",
    "occupation": "Ocupación",
    "message_value": "Valor del Mensaje",
    "call_value": "Valor de la Llamada",
    "video_call_value": "Valor de la videollamada",
    tokens:"tokens"
  },
  "buttons": {
    "submit": "Enviar",
    "cancel": "Cancelar",
    "submit_code": "Enviar código de verificación",
    "fill": "Recargar",
    "withdraw": "Redimir",
    "change_lang": "Cambiar idioma",
    "log_out": "Cerrar sesión",
    "update": "Actualizar",
    "see_more": "Ver más",
    "post_changes": "Publicar cambios",
    "resume": "Reanudar",
    "pause": "Pausar"
  },
  "alerts": {
    "lang_updated": "El idioma se actualizó correctamente",
    "success": "Bien",
    "internal_error": "¡Tenemos inconvenientes, inténtelo mas tarde!",
    "no_permissions": "No tienes permisos",
    "update_success": "La actualización se realizó correctamente",
    "form_errors": "Hay errores en el formulario. Por favor, corríjelos antes de continuar.",
    "no_records_found": "¡No se encontraron registros que coincidan con sus criterios!",
    "are_you_sure": "¿Estás seguro?",
    "change_occupation_to_pending": "La ocupación cambiará estado PENDIENTE y no podrás hacer más cambios",
    "delete_document": "¿Deseas eliminar este documento?",
    "logout": "¿Deseas cerrar sesión?",
    "delete_record": "¿Deseas eliminar este registro?"
  },
  "state": {
    "pending": "Pendiente",
    "rejected": "Rechazado",
    "approved": "Aprobado",
    "created": "Creado",
    "paused": "Pausado"
  },
  "pages": {
    "home": {
      "label": "Inicio",
      "earned_tokens": "Tokens ganados",
      "all_contracts": "Contratos realizados",
      "average_rating": "Calificación promedio"
    },
    "profile": {
      "label": "Perfil",
      "upload_image": "Subir imagen",
      "personal_info": "Información personal",
      "comments_and_ratings": "Comentarios y calificaciones",
      "languages": "Idiomas en los que presta servicio",
      "security": "Seguridad",
      "no_comments": "Aún no tienes ningún comentario."
    },
    "balance": {
      "label": "Saldo",
      "current_balance": "Saldo Actual",
      "tokens_since_withdrawal": "Transacciones de tokens desde el último retiro",
      "last_withdrawal": "Último retiro",
      "contracts": "Contratos",
      "refills": "Recargas",
      "withdrawals": "Retiros",
      "messages": "Mensajes",
      "calls": "Llamadas",
      "video_calls": "Videollamadas",
      "previous_balance": "Saldo Anterior",
      "occupation": "Ocupación",
      "date": "Fecha",
      "contract_type": "Tipo de Contrato",
      "spent_tokens": "Tokens gastados",
      "refill": "Recarga",
      "withdrawal": "Retiro",
      "call": "Llamada",
      "video_call": "Videollamada",
      "type": "Tipo"
    },
    "occupations": {
      "label": "Ocupaciones",
      "occupations_pending": "Ocupaciones Pendientes",
      "occupations_rejected": "Ocupaciones Rechazadas",
      "occupations_approved": "Ocupaciones Aprobadas",
      "occupations_paused": "Ocupaciones Pausadas",
      "occupations_created": "Ocupaciones Creadas",
      "occupations_all": "Todas las Ocupaciones",
      "add_occupation": "Agregar Ocupación",
      "edit_tags": "Editar Tags",
      "message_value": "Valor del Mensaje",
      "call_value": "Valor de la Llamada",
      "video_call_value": "Valor de la videollamada",
      "accept_negotiation": "Aceptar negociación de tarifas",
      "save_messages_history": "Guardar historial de mensajes",
      "documents": "Documentos",
      "document": "Documento",
      "contracts_by_month": "Contratos por mes",
      "documents_pending": "Hay algunos documentos pendientes"
    },
    "help": {
      "label": "Ayuda"
    },
    "footer": {
      "copyright": "Todos los derechos reservados",
      "about": "Somos una empresa que se dedica a actividades de desarrollo de sistemas informáticos"
    }
  },
  "yes":"Sí",
  "no": "No",
  "required": "Requerido",
  "status": "Status",
  "actions": "Acciones"
}
