import TokenFeeService from '@/services/TokenFeeService'
import TokenBuyService from '@/services/TokenBuyService'
import AuthService from "@/services/AuthService";
import ContractService from "@/services/ContractService";
import CountryService from "@/services/CountryService";
import OccupationService from "@/services/OccupationService";
import PaypalService from "@/services/PaypalService";
import UserOccupationService from "@/services/UserOccupationService";


export default ($axios) => ({
  TokenFeeService: TokenFeeService($axios),
  TokenBuyService: TokenBuyService($axios),
  AuthService: AuthService($axios),
  ContractService: ContractService($axios),
  CountryService: CountryService($axios),
  OccupationService: OccupationService($axios),
  PaypalService: PaypalService($axios),
  UserOccupationService: UserOccupationService($axios),
})
