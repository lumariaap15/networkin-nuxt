import axios from 'axios';
import {state} from "~/store";
import {getCookie} from "@/globals/globals";
//import router from "../router/router";
//import {store} from "../store";


axios.defaults.baseURL = 'http://networkin-backend.test/api';

//interceptamos para que siempre envie el token al post
axios.interceptors.request.use(function (config) {
    config.headers.common['languageIso'] = 'es';
    if (config.url !== '/api/auth/login') {
        const auth_token = getCookie("auth._token.laravelJWT");
        if (auth_token !== "") {
            config.headers.Authorization = auth_token;
        }
    }
    return config;
}, function (err) {
    return Promise.reject(err);
});

//Redireccionamos al login si el token expiró
/*
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 401 || error.response.status === 403) {
        if (error.response.config.url !== 'auth/login') {
            localStorage.clear();
            window.location.reload();
        }
    }
    return Promise.reject(error);
});

 */



//Redireccionamos si alguna petición nos envía un 404
/*
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 404) {
        window.location.href = '/404'
    }
    return Promise.reject(error);
});
 */

//console.log(store.state.locale);
//axios.defaults.headers.common['languageIso'] = store.state.locale;

export default axios
