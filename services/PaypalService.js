export default ($axios) => ({
    createPayment(data) {
        return $axios.post('paypal/createpayment',data);
    },
});
