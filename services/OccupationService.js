export default ($axios) => ({
    getTags(occupationId) {
        return $axios.get('occupations/'+occupationId+'/tags');
    },
    getDocuments(occupationId) {
        return $axios.get('occupations/'+occupationId+'/documents');
    },
    getOccupations(){
        return $axios.get('occupations');
    }
});
