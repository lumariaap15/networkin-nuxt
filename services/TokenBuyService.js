const base = 'tokenbuy';
export default ($axios) => ({
    authTotalTokens() {
        return $axios.get(base+'/total_tokens');
    },
    all(page){
        return $axios.get(base+'?page='+page);
    },
    tokensFromLastWithdrawal(){
        return $axios.get(base+'/transationsdetal');
    },
    deleteTokenBuy(id){
        return $axios.delete(base+'/delete/'+id);
    },
});


