export default ($axios) => ({
    login(data) {
        return $axios.post('auth/login',data);
    },
    register(data) {
        return $axios.post('auth/register',data);
    },
    logout() {
        return $axios.post('auth/logout');
    },
    recoverAccount(data){
        return $axios.post('auth/recoveraccount',data);
    },
    verifyCodeRecoverAccount(data){
        return $axios.post('auth/verifycoderecoveraccount',data);
    },
    updateInfo(data, multipart = false){
        let config = {};
        if (multipart){
            config = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        }
        return $axios.post('user/updateinfo',data, config);
    },
    updatePassword(data){
        return $axios.put('user/updatepassword',data);
    },
    loginGoogle(data){
        return $axios.post('auth/registerorlogingoogle',data)
    }
});
