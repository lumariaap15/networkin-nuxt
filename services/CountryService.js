export default ($axios) => ({
    all() {
        return $axios.get('countries');
    },
});
