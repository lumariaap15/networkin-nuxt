export default ($axios) => ({
    all(page) {
        return $axios.get('contracts?page='+page);
    },
    delete(contractId){
        return $axios.delete('contracts/'+contractId+'/delete')
    }
});
