export default ($axios) => ({
    authUserOccupations(withCount = false) {
        return $axios.get('user_occupation/occupations?'+(withCount?'count':''));
    },
    userOccupationComments(occupation_id,user_id, page){
        return $axios.get(`commentary/occupation/${occupation_id}/user/${user_id}?page=${page}`)
    },
    userOccupationPuntuation(occupation_id,user_id){
        return $axios.get(`scoredescription/occupation/${occupation_id}/user/${user_id}`)
    },
    authUserOccupationDetail(id) {
        return $axios.get('user_occupation/occupations/'+id);
    },
    updateUserOccupation(data){
        return $axios.put('user_occupation/updateoccupation',data);
    },
    updateState(occupationId, state){
        let data = {
          occupation_id: occupationId,
          state,
        };
        return $axios.put('user_occupation/updatestateoccupation',data);
    },
    addTags(data){
        return $axios.post('user_occupation/addtags',data);
    },
    deleteDocument(data){
        return $axios.delete('user_occupation/delete_document',{data});
    },
    addDocument(data){
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        };
        return $axios.post('user_occupation/adddocument',data, config);
    },
    updateDocument(data){
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        };
        data.append('_method','PUT');
        return $axios.post('user_occupation/updatedocument',data, config);
    },
    addUserOccupation(occupationId){
        return $axios.post('user_occupation/addoccupation',
            {occupation_id: occupationId});
    },
    userOccupationContractsHistory(userOccupationId, page){
        return $axios.get('user_occupation/contractshistorico/'+userOccupationId+'?page='+page);
    }
});
