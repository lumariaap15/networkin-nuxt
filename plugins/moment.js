import moment from 'moment';
import Vue from 'vue';

moment.locale('es');

Vue.use(require('vue-moment'), {moment});
