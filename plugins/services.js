import createRepository from '~/services/GlobalService2'

export default (ctx, inject) => {
  ctx.$axios.setBaseURL('http://networkin-backend.test/api');
  inject('services', createRepository(ctx.$axios))
}
