import { ValidationProvider, ValidationObserver } from 'vee-validate';
import Vue from 'vue';

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

import { extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';


// loop over all rules
for (let rule in rules) {
  extend(rule, {
    ...rules[rule], // add the rule
  });
}

