
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/sass/app.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    {src:'~/plugins/route'},
    {src:'~/plugins/moment'},
    {src:'~/plugins/vee-validate'},
    //{src:'~/plugins/google-oauth'},
    {src:'~/plugins/i18n'},
    {src:'~/plugins/vue-loaders'},
    {src:'~/plugins/vue-select'},
    {src:'~/plugins/vue-sweetalert2'},
    {src:'~/plugins/mixins'},
    {src:'~/plugins/eventBus.js'},
    {src:'~/plugins/services.js'},
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    'nuxt-i18n',
  ],

  auth: {
    strategies: {
      'laravelJWT': {
        provider: 'laravel/jwt',
        url: 'http://networkin-backend.test/api/',
        endpoints: {
          login: { url: 'auth/login', method: 'post' },
          logout: { url: 'auth/logout', method: 'post' },
          user: { url: 'auth/me', method: 'post' }
        },
        token: {
          property: 'access_token',
          maxAge: 60 * 60
        },
        refreshToken: {
          maxAge: 20160 * 60
        },
        user:{
          autoFetch: false
        }
      },
      google: {
        //client_id: '8840503382-1v3j7bnru9q71n03ur6rikt719teopjo.apps.googleusercontent.com',
        clientId: '912961361492-gp85ljurugjnio09s2tueb071c85msuv.apps.googleusercontent.com',
      },
      social: {
        scheme: 'oauth2',
        endpoints: {
          authorization: 'https://accounts.google.com/o/oauth2/auth',
          token: undefined,
          userInfo: 'https://www.googleapis.com/oauth2/v3/userinfo',
          logout: 'https://example.com/logout'
        },
        token: {
          property: 'access_token',
          type: 'Bearer',
          maxAge: 1800
        },
        refreshToken: {
          property: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30
        },
        responseType: 'id_token',
        grantType: 'authorization_code',
        accessType: undefined,
        redirectUri: undefined,
        logoutRedirectUri: undefined,
        //clientId: '8840503382-1v3j7bnru9q71n03ur6rikt719teopjo.apps.googleusercontent.com',
        clientId: '912961361492-gp85ljurugjnio09s2tueb071c85msuv.apps.googleusercontent.com',
        scope: ['openid', 'profile', 'email'],
        state: 'UNIQUE_AND_NON_GUESSABLE',
        codeChallengeMethod: '',
        responseMode: '',
        acrValues: '',
        // autoLogout: false
      }
    }
  },

  router: {
    //middleware: ['auth']
  },

  bootstrapVue: {
    icons: true
  },

  i18n: {
    locales: [
      {
        name: 'Italiano',
        code: 'it',
        file: 'it.js'
      },
      {
        name: 'English',
        code: 'en',
        file: 'en.js'
      },
      {
        name: 'Español',
        code: 'es',
        file: 'es.js'
      },
      {
        name: 'Français',
        code: 'fr',
        file: 'fr.js'
      },
      {
        name: 'Italiano',
        code: 'pt',
        file: 'pt.js'
      },
      {
        name: 'Deutsch',
        code: 'de',
        file: 'de.js'
      },
    ],
    langDir: 'assets/lang/',
    lazy: true,
    defaultLocale: 'es',
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    transpile:[
      'vee-validate',
      'vue-sweetalert2',
      'vue-google-oauth2'
    ]
  },

  server:{
    port: '8000',
    //host: '0.0.0.0'
  }
}
