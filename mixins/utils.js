export const utils = {
    computed: {
        /*
        graphColors(){
            let array =  [
                "#FFA125",
                "#00D700",
                "#009DC4",
                "#B97DFF",
                "#F2CA00",
                "#FF5762"
            ];
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        },
         */
        userAuth(){
          return this.$auth.user;
        },
    },
    methods: {
        errorAlert(error) {
            if (error.response) {
                let finalError = error.response;
                console.log(finalError)
                //this.showSpinner(false);
                if (finalError.status !== 422 && finalError.status !== 400 && finalError.status !== 403 && finalError.status !== 401) {
                    this.$swal.fire({
                        title: '¡Ops!',
                        text: this.$t('alerts.internal_error'),
                        icon: 'error',
                    }).then(() => {
                    });
                } else if (finalError.status === 400 || finalError.status === 401) {
                    this.$swal.fire({
                        title: '¡Ops!',
                        text: finalError.data.error,
                        icon: 'warning',
                    }).then(() => {
                    });
                } else if (finalError.status === 403) {
                    this.$swal.fire({
                        title: this.$t('alerts.no_permissions'),
                        text: finalError.data.error,
                        icon: 'warning',
                    }).then(() => {
                    });
                } else if (finalError.status === 422) {
                    this.$swal.fire({
                        title: '¡Ops!',
                        text: finalError.data[Object.keys(finalError.data)[0]][0],
                        icon: 'warning',
                    }).then(() => {
                    });
                }  else {
                    this.$swal.fire({
                        title: '¡Ops!',
                        text: finalError.data,
                        icon: 'warning',
                    }).then(() => {
                    });
                }
            } else {
                this.$swal.fire({
                    title: '¡Ops!',
                    text: error,
                    icon: 'error',
                }).then(() => {
                });
            }
        },
        successAlert(message) {
            return this.$swal.fire({
                title: this.$t('alerts.success'),
                text: '¡' + message + '!',
                icon: 'success',
            })
        },
        questionAlert(question) {
            return this.$swal.fire({
                title: this.$t('alerts.are_you_sure'),
                text: question,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'OK',
                cancelButtonText: this.$t('buttons.cancel')
            })
        },
        getTypeService(num){
            switch (Number(num)) {
                case 1:
                    return this.$t('pages.balance.messages');
                case 2:
                    return this.$t('pages.balance.call');
                default:
                    return this.$t('pages.balance.video_call');
            }
        },
        getOccupationStateVariant(state){
            switch (Number(state)) {
                case 1:
                    return "warning";
                case 2:
                    return "danger";
                case 3:
                    return "success";
                case 5:
                    return "info";
                case 6:
                    return "primary";
            }
        },
        getOccupationStateName(state){
            switch (Number(state)) {
                case 1:
                    return this.$t('state.pending');
                case 2:
                    return this.$t('state.rejected');
                case 3:
                    return this.$t('state.approved');
                case 5:
                    return this.$t('state.paused');
                case 6:
                    return this.$t('state.created');
            }
        },
        /*
        promptConfirmAlert() {
            return this.$swal.fire({
                title: 'Contraseña de confirmación',
                text: 'Debe Ingresar la su contraseña para realizar esta accion',
                input: 'password',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                showLoaderOnConfirm: true,
                preConfirm: (password) => {
                    return AuthService.cofirmPassword({password:password})
                },
               // allowOutsideClick: () => !Swal.isLoading()
               allowOutsideClick: false
            }).then((result) => {
                if(result.value) {
                    if (result.value.data.success) {
                        return true;
                    } else {
                        this.$swal.fire({
                            title: '¡Advertencia!',
                            text: '¡La contraseña ingresada no coincide!',
                            icon: 'warning',
                        })
                        return false;
                    }
                }
            })
        }
        */
    },

};
