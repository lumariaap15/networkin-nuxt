export const state = () => ({
  totalTokens:0,
  authUser:{},
  locale:'es'
})

export const mutations = {
  changeTotalTokens(state, totalTokens) {
    state.totalTokens = totalTokens;
  },
  changeAuthUser(state,user){
    state.authUser = user;
  },
  LOCALE(state, locale){
    state.locale = locale;
  }
}


export const actions = {
  nuxtServerInit({ commit }) {
    console.log(this.$i18n.locale)
    commit('LOCALE', this.$i18n.locale)
  }
}
